# Roger l'omnicorrecteur: un système de correction automatisée pour les scripts R... et bien plus!

Matériel d'une présentation effectuée dans le cadre des activités
d'échange pédagogique de la [Faculté des sciences et de génie](https://fsg.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

## Auteurs

Vincent Goulet, École d'actuariat, Université Laval  
David Beauchemin, Département d'informatique et de génie logiciel, Université Laval

## Résumé

Roger est un système de correction automatisée pour les travaux informatiques développé et utilisé activement à l’École d’actuariat. Spécialisé dans la correction de code R, Roger peut être adapté à d’autres langages de programmation interprétés ou compilés grâce à sa polyvalence et à sa modularité. Développé à l'origine pour accélérer la correction des travaux dans les grands groupes, Roger a aussi permis des innovations pédagogiques qui améliorent l'intégration des concepts par les étudiants.
