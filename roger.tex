%%% Copyright (C) 2022 David Beauchemin, Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Atelier d'échange pédagogique de
%%% la Faculté des sciences et de génie - Roger l'omnicorrecteur: un
%%% système de correction automatisée pour les scripts R... et bien
%%% plus!» https://gitlab.com/vigou3/atelier-pedagogique-fsg-roger
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0 International de
%%% Creative Commons. https://creativecommons.org/licenses/by-sa/4.0/

\section{Présentation de Roger}

\begin{frame}
  \frametitle{Contexte}

  \centering
  \setlength{\unitlength}{1mm}
  \begin{picture}(120,13)(0,-5)
    \put(0,1){\color{dark}\rule[-1mm]{120mm}{2mm}}
    \multiput(10,1)(20,0){6}{\circle*{4}}
    \put( 10,6){\makebox(0,0){2017}}
    \put( 30,6){\makebox(0,0){2018}}
    \put( 50,6){\makebox(0,0){2019}}
    \put( 70,6){\makebox(0,0){2020}}
    \put( 90,6){\makebox(0,0){2021}}
    \put(110,6){\makebox(0,0){2022}}

    \only<1>{\put( 10,1){\color{alert}\circle*{4}}}
    \only<2>{\put( 30,1){\color{alert}\circle*{4}}}
    \only<3>{\put( 50,1){\color{alert}\circle*{4}}}
    \only<4>{\put( 70,1){\color{alert}\circle*{4}}}
    \only<5>{\put( 90,1){\color{alert}\circle*{4}}}
    \only<6>{\put(110,1){\color{alert}\circle*{4}}}
  \end{picture}
  \bigskip

  \begin{minipage}{0.8\linewidth}
    \begin{overprint}
      \onslide<1>                 % 2017
        \begin{itemize}
        \item IFT-1902 Informatique pour actuaires maintenant basé sur R
        \item Critères de correction inspirés de
          \link{https://wiki.cs.astate.edu/index.php?title=Computer_Science_Program_Grading_Criteria&oldid=10507}{%
            Arkansas State University}:
          \begin{itemize}
          \item Justesse des résultats (R)
          \item Justesse du code et de l'algorithme (A)
          \item Style de codage (S)
          \item Documentation et commentaires (D)
          \end{itemize}
        \end{itemize}
      \onslide<2>                 % 2018
        \begin{itemize}
        \item Ajout d'un travail longitudinal individuel
        \item Exigences strictes pour la remise des travaux
        \item Premier script de correction
        \end{itemize}
      \onslide<3>                 % 2019
        \begin{itemize}
        \item Plusieurs erreurs de non respects des exigences
        \item Travaux compensatoires
        \item Premier script de validation
        \end{itemize}
      \onslide<4>                 % 2020
        \begin{itemize}
        \item Projet CAS prend forme
        \item Script de validation livré avec les travaux
        \item Script de correction utilisé couramment
        \item Développement d'outils de validation du style de codage
        \end{itemize}
      \onslide<5>                 % 2021
        \begin{itemize}
        \item Projet Roger structuré en deux volets:
          \begin{itemize}
          \item système de base
          \item paquetage R
          \end{itemize}
        \item Versions 0.99-0 à 0.99-19 entre septembre et avril
        \end{itemize}
      \onslide<6>                 % 2022
        \begin{itemize}
        \item \emph{Carte de correction} hiérarchique très flexible
        \item Modules de correction et de validation
        \item Versions 1.0-0 du système de base et du paquetage R
        \end{itemize}
    \end{overprint}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Qu'est-ce que c'est au juste?}

  \centering%
  Roger est un système d'exécution de \alert{tests unitaires} et de
  compilation des résultats

  \bigskip

  \LARGE%
  {\faList} \quad
  {\faArrowRight} \quad
  \raisebox{-0.5ex}{\includegraphics[height=3ex,keepaspectratio]{images/noun-fish-912638.png}} \quad
  {\faArrowRight} \quad
  {\faChartBar}
\end{frame}

\begin{frame}
  \frametitle{Choix technologiques}

  Roger est une collection de procédures d'interpréteur de commandes
  (\emph{shell scripts}) Unix.

  \begin{itemize}
  \item Multiplateforme
    \begin{itemize}
    \item macOS et Linux: standard
    \item Windows: interpréteur Bash livré avec Git (\emph{Git Bash})
    \end{itemize}
  \item Agnostique
  \item Plusieurs opérations dévolues au système d'exploitation
  \item Temps de correction $\gg$ temps de traitement
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Caractéristiques}

  \begin{itemize}
  \item Prise en charge des travaux remis via des \alert{dépôts Git}
  \item Carte de correction très \alert{flexible} exprimée dans une syntaxe
    similaire à YAML
  \item Outils de correction et de validation \alert{modulaires}
  \item \alert{Modules intégrés} de correction et de validation pour
    scripts R, documents R~Markdown, applications Shiny, paquetages R
    et procédures d’interpréteur de commandes
  \item Entièrement \alert{bilingue} (aide et messages)
  \item Guides de \alert{validation} et de \alert{correction} dans le
    \link{https://roger-project.gitlab.io}{site web}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Composantes du système de base}

  \begin{ttscript}{$\ast$\, \code{roger validate}}
  \item[\phantom{$\ast$\, }\bfseries\code{roger}] \textbf{interface unifiée
    vers les autres outils} \\[\baselineskip]
  \item[\phantom{$\ast$\, }\code{roger checkreq}] vérifie la disponibilité des outils de
    correction
  \item[\phantom{$\ast$\, }\code{roger clone}] clone en lot les travaux hébergés dans des
    dépôts Git
  \item[\textcolor{alert}{$\ast$}\, \code{roger grade}] corrige automatiquement les critères qui
    le permettent
  \item[\textcolor{alert}{$\ast$}\, \code{roger harvest}] recueille les résultats de la correction
  \item[\phantom{$\ast$\, }\code{roger push}] publie les résultats directement dans les
    dépôts Git
  \item[\phantom{$\ast$\, }\code{roger switch}] bascule en lot des dépôts Git de branche
  \item[\textcolor{alert}{$\ast$}\, \code{roger validate}] valide un travail avant d’en effectuer
    la remise
  \end{ttscript}
\end{frame}

\begin{frame}[standout]
  Démonstration
\end{frame}

\begin{frame}
  \frametitle{Prochaines étapes}

  \begin{itemize}
  \item Robustesse accrue
  \item Assistant d'installation
  \item Interface web ou graphique (?)
  \end{itemize}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "atelier-pedagogique-fsg-roger"
%%% End:
