### -*-Makefile-*- pour préparer "Roger l'omnicorrecteur: un système
### de correction automatisée pour les scripts R... et bien plus!"
##
## Copyright (C) 2022 David Beauchemin, Vincent Goulet
##
## Ce fichier fait partie du projet «Atelier d'échange pédagogique de
## la Faculté des sciences et de génie - Roger l'omnicorrecteur: un
## système de correction automatisée pour les scripts R... et bien
## plus!» <https://gitlab.com/vigou3/atelier-pedagogique-fsg-roger>
##
## Cette création est mise à disposition selon le contrat
## Attribution-Partage dans les mêmes conditions 4.0 International de
## Creative Commons. <https://creativecommons.org/licenses/by-sa/4.0>
##
## 'make pdf' compile le document maitre avec XeLaTeX.
##
## 'make demo' met en place les dépôts Git pour la démonstration;
## requiert 'bitbucketapi' <https://gitlab.com/vigou3/bitbucketapi>
##
## 'make release' crée une nouvelle version dans GitLab et téléverse
## le fichier .pdf.
##
## 'make check-url' vérifie la validité de toutes les url présentes
## dans les sources du document.
##
## 'make all' est équivalent à 'make pdf' question d'éviter les
## publications accidentelles.
##
## Auteur: Vincent Goulet

## Principaux fichiers
MASTER = atelier-pedagogique-fsg-roger.pdf
README = README.md
NEWS = NEWS
LICENSE = LICENSE

## Le document maitre dépend de tous les fichiers .tex autres que
## lui-même.
TEXFILES = $(addsuffix .tex,$(filter-out $(basename ${MASTER}),\
                                         $(basename $(wildcard *.tex))))

## Hébergement Git de la démonstration
PROJECT = a2022-85831
PROJECTURL = $(shell cat ~/.bitbucketrc | cut -d " " -f 2)

## Informations de publication extraites du fichier maitre
TITLE = $(shell grep "\\\\title" ${MASTER:.pdf=.tex} \
	| cut -d { -f 2 | tr -d })
REPOSURL = $(shell grep "newcommand{\\\\reposurl" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
YEAR = $(shell grep "newcommand{\\\\year" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
MONTH = $(shell grep "newcommand{\\\\month" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
VERSION = ${YEAR}.${MONTH}

## Outils de travail
TEXI2DVI = LATEX=xelatex TEXINDY=makeindex texi2dvi -b
CP = cp -p
RM = rm -rf
MD = mkdir -p

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variables automatiques
TAGNAME = v${VERSION}


all: pdf

FORCE:

${MASTER}: ${MASTER:.pdf=.tex} ${TEXFILES} $(wildcard images/*)
	${TEXI2DVI} ${MASTER:.pdf=.tex}

.PHONY: demo-ok
demo-ok:
	if [ -d demo/555000000_demo ]; then ${RM} demo/555000000_demo; fi
	if bitbucketapi getrepos ${PROJECT} | grep -q 555000000_demo; \
	then \
	    bitbucketapi delrepos ${PROJECT} 555000000_demo; \
	fi
	bitbucketapi addrepos ${PROJECT} 555000000_demo
	git clone https://${PROJECTURL}/scm/${PROJECT}/555000000_demo.git demo/555000000_demo
	${CP} demo/hello-ok.txt demo/555000000_demo/hello.txt
	${CP} demo/foo-ok.R demo/555000000_demo/foo.R
	${CP} demo/bar-ok.R demo/555000000_demo/bar.R
	${CP} demo/validateconf demo/555000000_demo
	( \
	    cd demo/555000000_demo; \
	    git switch -c master; \
	    git add hello.txt foo.R bar.R validateconf; \
	    git commit -m "Ajouter les fichiers pour la démonstration"; \
	    git push -u origin master; \
	)
	${RM} demo/555000000_demo

.PHONY: demo-fixme
demo-fixme:
	if [ -d demo/555000001_demo ]; then ${RM} demo/555000001_demo; fi
	if bitbucketapi getrepos ${PROJECT} | grep -q 555000001_demo; \
	then \
	    bitbucketapi delrepos ${PROJECT} 555000001_demo; \
	fi
	bitbucketapi addrepos ${PROJECT} 555000001_demo
	git clone https://${PROJECTURL}/scm/${PROJECT}/555000001_demo.git demo/555000001_demo
	${CP} demo/hello-fixme.txt demo/555000001_demo/hello.txt
	${CP} demo/foo-fixme.R demo/555000001_demo/foo.R
	${CP} demo/bar-fixme.R demo/555000001_demo/bar.R
	${CP} demo/validateconf demo/555000001_demo
	( \
	    cd demo/555000001_demo; \
	    git switch -c master; \
	    git add hello.txt foo.R bar.R validateconf; \
	    git commit -m "Ajouter les fichiers pour la démonstration"; \
	    git push -u origin master; \
	)
	${RM} demo/555000001_demo

.PHONY: demo
demo: demo-ok demo-fixme

.PHONY: pdf
pdf: ${MASTER}

.PHONY: release
release: pdf check-status upload create-release

.PHONY: check-status
check-status:
	@echo ----- Checking status of working directory...
	@if [ "master" != $(shell git branch --list | grep ^* | cut -d " " -f 2-) ]; then \
	     echo "not on branch master"; exit 2; fi
	@if [ -n "$(shell git status --porcelain | grep -v '^??')" ]; then \
	     echo "uncommitted changes in repository; not creating release"; exit 2; fi
	@if [ -n "$(shell git log origin/master..HEAD | head -n1)" ]; then \
	    echo "unpushed commits in repository; pushing to origin"; \
	     git push; fi

.PHONY: upload
upload :
	@echo ----- Uploading archive to GitLab...
	$(eval upload_url=$(shell curl --form "file=@${MASTER}" \
	                                        --header "PRIVATE-TOKEN: ${OAUTHTOKEN}"	\
	                                        --silent \
	                                        ${APIURL}/uploads \
	                                   | awk -F '"' '{ print $$8 }'))
	@echo url to file:
	@echo "${upload_url}"
	@echo ----- Done uploading files

.PHONY: create-release
create-release :
	@echo ----- Creating release on GitLab...
	if [ -e relnotes.in ]; then rm relnotes.in; fi
	touch relnotes.in
	awk 'BEGIN { ORS = " "; print "{\"tag_name\": \"${TAGNAME}\"," } \
	      /^$$/ { next } \
	      (state == 0) && /^# / { \
		state = 1; \
		out = $$2; \
	        for(i = 3; i <= NF; i++) { out = out" "$$i }; \
	        printf "\"name\": \"Édition %s\", \"description\":\"", out; \
	        next } \
	      (state == 1) && /^# / { exit } \
	      state == 1 { printf "%s\\n", $$0 } \
	      END { print "\",\"assets\": { \"links\": [{ \"name\": \"${MASTER}\", \"url\": \"${REPOSURL}${upload_url}\" }] }}" }' \
	     ${NEWS} >> relnotes.in
	curl --request POST \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master"
	curl --request POST \
	     --data @relnotes.in \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --header "Content-Type: application/json" \
	     --output /dev/null --silent \
	     ${APIURL}/releases
	${RM} relnotes.in
	@echo ----- Done creating the release

.PHONY: check-url
check-url: ${MASTER:.pdf=.tex} ${SCRIPTS}
	@echo ----- Checking urls in sources...
	$(eval url=$(shell grep -E -o -h 'https?:\/\/[^./]+(?:\.[^./]+)+(?:\/[^ ]*)?' $? \
		   | cut -d \} -f 1 \
		   | cut -d ] -f 1 \
		   | cut -d '"' -f 1 \
		   | sort | uniq \
	           | sed "s/.*/'&'/"))
	for u in ${url}; \
	    do if curl --output /dev/null --silent --head --fail --max-time 5 $$u; then \
	        echo "URL exists: $$u"; \
	    else \
		echo "URL does not exist (or times out): $$u"; \
	    fi; \
	done

.PHONY: clean
clean:
	${RM} ${MASTER} \
	      *.aux *.log *.snm *.nav *.vrb *.toc *.out
